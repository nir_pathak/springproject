package com.test.sp;

import java.awt.List;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonArrayFormatVisitor;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.test.sp.dao.CurrencyDao;
import com.test.sp.dao.CurrencyDaoImpl;
import com.test.sp.model.Currency;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	@Autowired
	protected CurrencyDao cdao;
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	/**
	 * Simply selects the home view to render by returning its name.
	 * 
	 * @param product
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {

		saveCurrencyData();
		model.addAttribute("currencyList",cdao.getAllCurrecny());

		return "home";
	}

	private void saveCurrencyData() {
		Gson gson = new Gson();

		String url = "http://data.fixer.io/api/latest?access_key=3b7918194e32de803e05ea1bdaa3a322&format=1";
		// RestTest rest= new RestTest();
		RestTemplate restTemplate = new RestTemplate();
		String resp = restTemplate.getForObject(url, String.class);

		Currency currency = gson.fromJson(resp, Currency.class);
		cdao.saveCurrencyRate(currency);
	}

}
