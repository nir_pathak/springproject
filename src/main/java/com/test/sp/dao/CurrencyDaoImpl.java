package com.test.sp.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.test.sp.model.Currency;

@Repository
public class CurrencyDaoImpl implements CurrencyDao {

	@Resource
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public void saveCurrencyRate(Currency currency) {

		Session session = sessionFactory.getCurrentSession();
		session.save(currency);
	}

	@Override
	@Transactional
	public List<Currency> getAllCurrecny() {
		Session session = sessionFactory.getCurrentSession();
		Criteria ctr = session.createCriteria(Currency.class);

		return ctr.list();
	}

}
