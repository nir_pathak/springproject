package com.test.sp.dao;

import java.util.List;

import com.test.sp.model.Currency;

public interface CurrencyDao {
	void saveCurrencyRate(Currency currency);
	List<Currency>  getAllCurrecny();

}
