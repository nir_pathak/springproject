package com.test.sp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="rates")
public class Rates {
	@Id
	@GeneratedValue
	private int id;
	private String AED;
	private String AFN;
	private String  AMD;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAED() {
		return AED;
	}
	public void setAED(String aED) {
		AED = aED;
	}
	public String getAFN() {
		return AFN;
	}
	public void setAFN(String aFN) {
		AFN = aFN;
	}
	public String getAMD() {
		return AMD;
	}
	public void setAMD(String aMD) {
		AMD = aMD;
	}

}
