<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
<style>
table, td, th {
  border: 1px solid black;
}

table {
  border-collapse: collapse;
  width: 100%;
}

th {
  text-align: left;
}
</style>
</head>
<body>
	<h1 style="center" >Currency Rate</h1>


	<table style="alian:center;">

		<tr>
			<td>Date</td>
			<td>Base</td>
			<td>AED</td>
			<td>AFN</td>
			<td>AMD</td>
		</tr>

		<c:forEach var="currency" items="${currencyList}">

			<tr>
				<td>${currency.date }</td>
				<td>${currency.base }</td>
				<td>${currency.rates.AED }</td>
				<td>${currency.rates.AFN }</td>
				<td>${currency.rates.AMD }</td>
			</tr>

		</c:forEach>



	</table>


</body>
</html>
